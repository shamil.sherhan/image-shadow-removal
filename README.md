# Image Shadow Removal

This project helps to remove shadows and apply uniform illumination across the image to make the image denoised while preserving the edges. 
The approach here is to find the dominant colors using unsupervised learning and applying it throughout the color range. 

Created using OpenCV and scikit-learn.


**P.S.**: *Part of a hackathon challenge. Not ideal on complex images.* 